package com.br.tictactoe.service.exception;

public class StandardException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public StandardException(String message) {
		super(message);
	}
}
