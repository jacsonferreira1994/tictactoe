package com.br.tictactoe.dto;

import java.io.Serializable;

import com.br.tictactoe.domain.Game;

import lombok.Getter;

public class GameDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private @Getter String id;
	private @Getter char firstPlayer;

	public GameDTO(Game game) {
		id = game.getId();
		firstPlayer = game.getCurrentPlayerMark().asChar();
	}

}
