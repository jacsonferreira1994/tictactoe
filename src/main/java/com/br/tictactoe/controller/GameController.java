package com.br.tictactoe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.tictactoe.dto.GameDTO;
import com.br.tictactoe.dto.MovementDTO;
import com.br.tictactoe.dto.ResultDTO;
import com.br.tictactoe.service.GameService;

@RestController
@RequestMapping("/game")
public class GameController {
	@Autowired
	private GameService gameService;

	@PostMapping
	public ResponseEntity<GameDTO> create() {
		return ResponseEntity.ok().body(gameService.newGame());
	}

	@PostMapping("{id}/movement")
	public ResponseEntity<ResultDTO> doMovement(@RequestBody MovementDTO movement) {	
		return ResponseEntity.ok().body(gameService.movement(movement));
	}
}
