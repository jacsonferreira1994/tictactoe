package com.br.tictactoe.domain;

public enum CurrentPlayerMark {
	X('X'), O('O');

	private final char asChar;

	public char asChar() {
		return asChar;
	}

	CurrentPlayerMark(char asChar) {
		this.asChar = asChar;
	}
}
