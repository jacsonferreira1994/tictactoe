package com.br.tictactoe.dto;

import lombok.Getter;

public class PositionDTO {

	private @Getter int x;
	
	private @Getter int y;

	public PositionDTO() {
	
	}
}
