package com.br.tictactoe.integration.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.br.tictactoe.domain.Game;

@Repository
public interface IGameRepository extends JpaRepository<Game, String> {

}
