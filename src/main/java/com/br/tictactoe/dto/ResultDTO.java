package com.br.tictactoe.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultDTO {
	private @Setter @Getter String msg;
	private @Setter @Getter String winner;

	public ResultDTO(String msg, String winner) {
		this.msg = msg;
		this.winner = winner;
	}

	public ResultDTO(String msg) {
		this.msg = msg;
	}
}
