package com.br.tictactoe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.tictactoe.domain.Game;
import com.br.tictactoe.dto.GameDTO;
import com.br.tictactoe.dto.MovementDTO;
import com.br.tictactoe.dto.ResultDTO;
import com.br.tictactoe.integration.data.repositories.IGameRepository;
import com.br.tictactoe.service.exception.ObjectNotFoundException;
import com.br.tictactoe.service.exception.StandardException;

@Service
public class GameService {

	@Autowired
	private IGameRepository gameRepository;

	public GameDTO newGame() {
		Game game = new Game();
		this.gameRepository.save(game);

		return new GameDTO(game);
	}

	public ResultDTO movement(MovementDTO movement) {
		ResultDTO result;
		Game game = this.gameRepository.findById(movement.getId()).orElseThrow(() -> new ObjectNotFoundException(
				"Cannot find object id: " + movement.getId() + " , type: " + Game.class.getName()));
		if (isYourTurn(game, movement)) {
			result = createMovement(game, movement);
		} else {
			throw new StandardException("isn't your turn");
		}

		return result;
	}

	private boolean isYourTurn(Game game, MovementDTO movement) {
		return (game.getCurrentPlayerMark() == movement.getCurrentPlayer());
	}

	public ResultDTO processResult(Game game, MovementDTO movement) {
		ResultDTO result;
		if (game.checkForWin()) {
			char player = movement.getCurrentPlayer().asChar();
			game.setResult(String.valueOf(player));
			gameRepository.save(game);
			result = new ResultDTO("We have a winner!", String.valueOf(player));
		} else if (game.isBoardFull()) {
			result = new ResultDTO("game finished", "Draw");
		} else {
			result = new ResultDTO("success move");
		}
		return result;
	}

	private ResultDTO createMovement(Game game, MovementDTO movement) {
		ResultDTO result;
		if (game.placeMark(movement.getPosition().getX(), movement.getPosition().getY())) {
			this.gameRepository.save(game);
			result = processResult(game, movement);
		} else {
			throw new StandardException("invalid move");
		}
		return result;
	}
}
