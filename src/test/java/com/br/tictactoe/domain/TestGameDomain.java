package com.br.tictactoe.domain;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.Test;

import com.br.tictactoe.domain.Game;

public class TestGameDomain {
	private Game game;

	@Test
	public void deveCriarUmJogo() {
		GenerateNewGame();
		assertNotEquals(null, game);
	}

	@Test
	public void deveCriarUmNovoBoard() {
		GenerateNewGame();
		int numberOfcolumns = game.getBoard().length;
		int numberOfRows = game.getBoard()[0].length;

		assertEquals(3, numberOfcolumns);
		assertEquals(3, numberOfRows);
	}

	@Test
	public void validaSerboadEstafull() {
		GenerateNewGame();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				game.getBoard()[i][j] = 'X';
			}
		}
		assertTrue(game.isBoardFull());
	}

	@Test
	public void checkforwin() {
		GenerateNewGame();
		game.placeMark(0, 0);
		game.placeMark(0, 1);
		game.placeMark(1, 0);
		game.placeMark(1, 1);
		game.placeMark(2, 0);
		game.checkForWin();

		assertTrue(game.checkForWin());
	}

	public void GenerateNewGame() {
		game = new Game();
	}
}
