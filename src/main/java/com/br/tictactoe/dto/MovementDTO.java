package com.br.tictactoe.dto;

import com.br.tictactoe.domain.CurrentPlayerMark;

import lombok.Getter;

public class MovementDTO {

	private @Getter String id;

	private @Getter CurrentPlayerMark currentPlayer;

	private @Getter PositionDTO position;
}
